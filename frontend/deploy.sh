#! /bin/bash

#Если свалится одна из команд, рухнет и весь скрипт
set -xe

#Перезаливаем дескриптор сервиса на ВМ для деплоя
sudo cp -rf sausage-store-frontend.service /etc/systemd/system/sausage-store-frontend.service
sudo rm -rf /home/jarservice/frontend||true
sudo rm  /home/jarservice/frontend.tar.gz||true

#Переносим артефакт в нужную папку
curl -u ${NEXUS_REPO_USER}:${NEXUS_REPO_PASS} -o frontend.tar.gz ${NEXUS_REPO_URL}/repository/sausage-store-antonova-olga-frontend/${VERSION}/sausage-store-${VERSION}.tar.gz
sudo mkdir /home/jarservice/frontend
sudo tar -xvzf frontend.tar.gz -C /home/jarservice/frontend
sudo cp -rf /home/jarservice/frontend/public_html/* /var/www-data/dist/frontend/
sudo chown -R front-user:front-user /var/www-data/dist/frontend

#Обновляем конфиг systemd с помощью рестарта
sudo systemctl daemon-reload

#Перезапускаем сервис сосисочной
sudo systemctl restart sausage-store-frontend.service 
