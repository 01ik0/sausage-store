ALTER TABLE public.product ALTER COLUMN name TYPE varchar(20);

ALTER TABLE public.product ALTER COLUMN picture_url TYPE varchar(100);

-- add index for better performance
CREATE INDEX idx_order_id ON public.order_product USING btree (order_id, product_id);

-- add index for better performance
CREATE INDEX idx_o_id ON public.orders USING btree (id);

-- add index for better performance
CREATE INDEX idx_p_id ON public.product USING btree (id);

