#!/bin/bash
set +e

cat > .env <<EOF
VERSION=${VERSION}
DEV_HOST=${DEV_HOST}
PSQL_DATASOURCE_USERNAME=${PSQL_DATASOURCE_USERNAME}
PSQL_DATASOURCE_PASSWORD=${PSQL_DATASOURCE_PASSWORD}
PSQL_DATASOURCE_URL=${PSQL_DATASOURCE_URL}
CI_REGISTRY_USER=${CI_REGISTRY_USER}
CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD}
CI_REGISTRY=${CI_REGISTRY}
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE}
EOF

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker pull $CI_REGISTRY_IMAGE/sausage-backend:$VERSION

if [[ $(docker container inspect -f '{{.State.Running}}' backend-blue) = true ]]
then
    echo "Blue continer is now running"
    #docker-compose up -d --force-recreate backend-green
    docker compose up -d --pull always backend-green 
    green_status=$(docker container inspect -f '{{.State.Health.Status}}' backend-green)
    until [ "$green_status" = "healthy" ]
    do
      green_status=$(docker container inspect -f '{{.State.Health.Status}}' backend-green)
      echo $green_status
      sleep 10
    done
    echo "Green container is up!"
    docker stop backend-blue

elif [[ $(docker container inspect -f '{{.State.Running}}' backend-green) = true ]]
then
    echo "Green contianer is now running"
    #docker-compose up -d --force-recreate backend-blue
    docker compose up -d --pull always backend-blue
    blue_status=$(docker container inspect -f '{{.State.Health.Status}}' backend-blue)
    until [ "$blue_status" = "healthy" ]
    do
      blue_status=$(docker container inspect -f '{{.State.Health.Status}}' backend-blue)
      echo $blue_status
      sleep 10
    done
    echo "Blue container is up!"
    docker stop backend-green
else
    # Starting blue if all container is inactive
    echo "All backend containers is inacive. Start blue..." 
    docker-compose up -d --force-recreate backend-blue
    blue_status=$(docker container inspect -f '{{.State.Health.Status}}' backend-blue)
    until [ "$blue_status" = "healthy" ]
    do
      blue_status=$(docker container inspect -f '{{.State.Health.Status}}' backend-blue)
      echo $blue_status
      sleep 10
    done
    echo "Blue container is up!"         
fi


