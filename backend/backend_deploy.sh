#!/bin/bash
set +e

cat > .env <<EOF
SPRING_DATASOURCE_USERNAME=${SPRING_DATASOURCE_USERNAME}
SPRING_DATASOURCE_PASSWORD=${SPRING_DATASOURCE_PASSWORD}
SPRING_DATASOURCE_MONGODB_URI=${SPRING_DATASOURCE_MONGODB_URI}
SPRING_DATASOURCE_URL=${PSQL_DATASOURCE}
MONGO_DATA=${MONGO_DATA}
EOF

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker-compose stop backend
docker-compose rm -f backend
docker-compose up -d backend

#docker network create -d bridge sausage_network || true
#docker pull $CI_REGISTRY_IMAGE/sausage-backend:latest
#docker stop backend || true
#docker rm backend || true
#set -e
#docker run -d --name backend \
#    --network=sausage_network \
#    --restart always \
#    --pull always \
#    --env-file .env \
#    $CI_REGISTRY_IMAGE/sausage-backend:latest

