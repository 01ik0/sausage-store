#! /bin/bash

# если свалится одна из команд, рухнет и весь скрипт
set -xe

# перезаливаем дескриптор сервиса на ВМ для деплоя
sudo cp -rf /home/gitlab/sausage-store-backend.service /etc/systemd/system/sausage-store-backend.service

# убрать мусор за собой
sudo rm sausage-store-backend.service
sudo rm -f /home/jarservice/sausage-store.jar||true

# переносим артефакт в нужную папку
curl -u "${NEXUS_REPO_USER}:${NEXUS_REPO_PASS}" -o sausage-store.jar ${NEXUS_REPO_URL}/repository/sausage-store-antonova-olga-backend/com/yandex/practicum/devops/sausage-store/${VERSION}/sausage-store-${VERSION}.jar
sudo -S cp ./sausage-store.jar /home/jarservice/sausage-store.jar||true #"jar||true" говорит, если команда обвалится — продолжай

# обновляем конфиг systemd с помощью рестарта
sudo systemctl daemon-reload

# перезапускаем сервис сосисочной
sudo systemctl restart sausage-store-backend.service
