#!/bin/bash
set +e
docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
docker compose up -d --pull always backend-report
set -e

#docker-compose stop backend-report
#docker-compose up -d backend-report
#docker pull $CI_REGISTRY_IMAGE/backend-report:latest
#set -e
#docker run -d --name backend-report \
#    -ti -e PORT=8082 \
#    -e DB=${MONGO_DATA} \
#    --network=sausage_network \
#    --restart always \
#    --pull always \
#    -p 8082:8082 \
#    $CI_REGISTRY_IMAGE/backend-report:latest 
